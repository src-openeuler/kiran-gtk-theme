Name:		kiran-gtk-theme
Version:	2.6.0
Release:	3
Summary:	GTK+ theme designed for kiran

License:	LGPLv2+
Source0:        %{name}-%{version}.tar.gz

Patch0000:      0000-fix-gnomesoftware-Fix-style-error-of-install-and-lau.patch

BuildRequires:  cmake >= 3.2
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  sassc
%if ( 0%{?kylin_major_version} == 3 && 0%{?kylin_minor_version} == 3 && "%{?ks_installclass}" == "Server"  ) 
BuildRequires:  python36
BuildRequires:  python36-cairo
%else
BuildRequires:  python%{python3_pkgversion}
BuildRequires:  python%{python3_pkgversion}-cairo
%endif

BuildArch:      noarch

%description
The kiran-gtk-theme package contains the standard theme for the kiran desktop, which provides default appearance for window borders and  GTK+ applications.

%prep
%autosetup -p1

%build
%cmake
%cmake_build

%install
%cmake_install

 
%files
%{_datadir}/themes/Kiran-white/
%{_datadir}/themes/Kiran-dark/

%changelog
* Tue Nov 12 2024 Funda Wang <fundawang@yeah.net> - 2.6.0-3
- adopt to new cmake macro

* Mon Mar 25 2024 yinhongchang <yinhongchang@kylinsec.com.cn> - 2.6.0-2
- KYOS-B:Fix style error of install and launch button on gnome-software

* Mon Jan 15 2024 yuanxing <yuanxing@kylinos.com.cn> - 2.6.0-1.kb1
- KYOS-F: change the light theme name to Kiran-white.(#24747)

* Mon Dec 25 2023 yanglan <yanglan@kylinsec.com.cn> - 2.4.0-1.kb3
- rebuild for KiranUI-2.6-next

* Wed Dec 06 2023 liuxinhao <liuxinhao@kylinsec.com.cn> - 2.4.0-1.kb1
- release 2.4

* Tue Jun 20 2023 kpkg <kpkg.kylinsec.com.cn> - 2.3.0-6.kb1
- rebuild for KiranUI-2.5-next

* Fri May 5 2023 yinhongchang <yinhongchang@kylinos.com.cn> - 2.3.0-6
- KYOS-F: Fix compile gtk3.0 warning.

* Wed Apr 12 2023 kpkg <kpkg@kylinos.com.cn> - 2.3.0-5
- rebuild for KY3.4-GC-KiranUI-2.5

* Mon Apr 03 2023 kpkg <kpkg@kylinos.com.cn> - 2.3.0-4
- rebuild for KiranUI-2.5-next

* Mon Apr 03 2023 kpkg <kpkg@kylinos.com.cn> - 2.3.0-3
- rebuild for KiranUI-2.5-next

* Thu Oct 13 2022 tangjie02 <tangjie02@kylinsec.com.cn> - 2.3.0-2
- KYOS-F: Delete transition-duration for flat button.

* Tue Aug 23 2022 wxq <wangxiaoqing@kylinos.com.cn> - 2.3.0-1
- update to 2.3.0

* Tue Jan 11 2022 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.1-3.kb2
- rebuild for KY3.4-5-KiranUI-2.2

* Wed Dec 29 2021 kpkg <kpkg@kylinos.com.cn> - 2.2.1-3.kb1
- rebuild for KY3.4-MATE-modules-dev

* Wed Dec 29 2021 caoyuanji<caoyuanji@kylinos.com.cn> - 2.2.1-3
- Upgrade version number for easy upgrade

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.1-2.kb2
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.1-2.kb1
- rebuild for KY3.4-4-KiranUI-2.2

* Tue Dec 07 2021 wxq <wangxiaoqing@kylinos.com.cn> - 2.2.1-2.ky3
- KYOS-F: Use the old kiran gtk2 theme.

* Thu Nov 25 2021 wxq <wangxiaoqing@kylinos.com.cn> - 2.2.1.ky3
- KYOS-F: Add kiran dark and light theme.

* Wed Nov 03 2021 tangjie02 <tangjie02@kylinos.com.cn> - 2.2.0.ky3
- KYOS-B: New kiran theme.

* Mon May 10 2021 kpkg <kpkg@kylinos.com.cn> - 0.8.1-7+git20201105.93b21dd.kb3
- rebuild for KY3.4-4

* Thu Nov 05 2020 songchuanfei <songchuanfei@kylinos.com.cn> - 0.8.1-7+git20201105.93b21dd.ky3.kb2
- KYOS-F: rebuild

* Thu Nov 05 2020 songchuanfei <songchuanfei@kylinos.com.cn> - 0.8.1-7+git20201105.93b21dd.ky3.kb1
- KYOS-F: change systemtray icon size to 16px

* Fri Oct 09 2020 zhoujieda <zhoujieda@kylinos.com.cn> - 0.8-1-6.ky3.kb1
- KYOS-F: modify window manager and gtkheaderbar dialog(#29827)

* Thu Sep 03 2020 zhoujieda <zhoujieda@kylinos.com.cn> - 0.8-1-5.ky3.kb1
- KYOS-F: fix gnome-software's window manager dose not change with theme.(#29345)
- KYOS-F: fix kylin-license dialog has not close button.(#29284)  

* Tue Aug 04 2020 zhoujieda <zhoujieda@kylinos.com.cn> - 0.8-1-4.ky3.kb1
- KYOS-F: add old kiran theme and change new theme name

* Tue Jul 28 2020 zhoujieda <zhoujieda@kylinos.com.cn> - 0.8-1-3.ky3.kb2
- KYOS-F: change the window manager layout 

* Thu Jul 23 2020 zhoujieda <zhoujieda@kylinos.com.cn> - 0.8.1-3.ky3.kb1
- KYOS-F: add kiran-dark theme and change window manager button.(Related: #27538) 

* Thu May 28 2020 songchuanfei <songchuanfei@kylinos.com.cn> - 0.8.1-2.ky3.kb1
- KYOS-F: Specify conflicted package version for kiran-themes

* Thu May 28 2020 songchuanfei <songchuanfei@kylinos.com.cn> - 0.8.1-1.ky3.kb1
- Initial source for 0.8.1
